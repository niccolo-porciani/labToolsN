#!/bin/python3
# -*- coding:utf-8 -*-

import numpy as np
from uncertainties import ufloat
import uncertainties.unumpy as un
import re
import yaml
from io import StringIO
import collections.abc

ufloat_pattern = re.compile(r'[0-9\.] \+- [0-9\.]')

loader = yaml.SafeLoader

# Modifica il loader yaml per permettere sintassi più ampia sui float
# Fonte: https://stackoverflow.com/a/30462009
floatRegex = u'''(?:
     [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
    |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
    |\\.[0-9_]+(?:[eE][-+][0-9]+)?
    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
    |[-+]?\\.(?:inf|Inf|INF)
    |\\.(?:nan|NaN|NAN))'''

loader.add_implicit_resolver(
    u'tag:yaml.org,2002:float',
    re.compile(u'^' + floatRegex + u'$', re.X),
    list(u'-+0123456789.'))

# Modifica il loader yaml per leggere ufloat
# Crea un costruttore per gli ufloat
def ufloat_constructor(loader, node):
    value = loader.construct_scalar(node)
    central, sigma = map(float, value.split('+-'))
    return ufloat(central, sigma)

# Aggiunge al loader la possibilità di leggere ufloat
loader.add_constructor(u'!ufloat', ufloat_constructor)
loader.add_implicit_resolver(
    u'!ufloat',
    re.compile(
        u'^(?:' + floatRegex + u'|(?:[0-9]*))(?:\\s*)\\+-(?:\\s*)' +
        u'(?:' + floatRegex + u'|(?:[0-9]*))$',
        re.X),
    list(u'-+0123456789.'))

class dms:
    """Rappresenta angoli come gradi, minuti, secondi"""
    def __init__(self, d=0., m=0., s=0.):
        self.d = d
        self.m = m
        self.s = s

    def __repr__(self):
        return "{:.2f}° {:.2f}' {:.2f}\"".format(self.d,self.m,self.s)

    @classmethod
    def fromRadians(cls, r):
        r = r % (2 * np.pi)
        decimalDegrees = r * 360 / (2 * np.pi)
        d = np.floor(decimalDegrees)
        decimalMinutes = decimalDegrees - d
        m = np.floor(decimalMinutes * 60)
        s = (decimalMinutes - m / 60) * 3600
        print(type(d))
        return cls(d,m,s)

    def toRadians(self):
        return (self.d + self.m / 60 + self.s / 3600) * 2 * np.pi / 360

class udms():
    """Coppia di dms che rappresenta un valore con incertezza"""
    def __init__(self, nominal, sigma):
        self.nominal = nominal
        self.sigma = sigma

    def __repr__(self):
        return self.nominal.__repr__() + " +- " + self.sigma.__repr__()

    def toRadians(self):
        return ufloat(self.nominal.toRadians(), self.sigma.toRadians())

# Aggiunge al loader la possibilità di leggere dms
# ATTENZIONE: l'utilizzo di floatRegex qui non è davvero corretto. Meh.
dmsRegex = u'(?!$)' + \
    u'((?P<d>' + floatRegex + u'|(?:[0-9]*))(?:\\s?)(°|Â°)(?:\\s?))?' + \
    u'((?P<m>' + floatRegex + u'|(?:[0-9]*))(?:\\s?)\'(?:\\s?))?' + \
    u'((?P<s>' + floatRegex + u'|(?:[0-9]*))(?:\\s?)\"(?:\\s?))?'

def dmsFromString(value):
    matchDict = re.match(re.compile(dmsRegex, re.X), value).groupdict()
    matchDict = dict(
        map(lambda kvp: (kvp[0], float(kvp[1])),
            filter(lambda kvp: kvp[1] != None,
                matchDict.items())))
    return dms(**matchDict)

def dms_constructor(loader, node):
    value = loader.construct_scalar(node)
    return dmsFromString(value)

loader.add_constructor(u'!dms', dms_constructor)
loader.add_implicit_resolver(
    u'!dms',
    re.compile(u'^' + dmsRegex + u'$', re.X),
    list(u'-+0123456789.'))

# Aggiunge al loader la possibilità di leggere udms
def udms_constructor(loader, node):
    value = loader.construct_scalar(node)
    central, sigma = map(
        lambda s: dmsFromString(s.strip()),
        value.strip().split('+-'))
    return udms(central, sigma)

loader.add_constructor(u'!udms', udms_constructor)
loader.add_implicit_resolver(
    u'!udms',
    re.compile(u'^' + dmsRegex + u'(?:\\s*)\\+-(?:\\s*)' +
        dmsRegex.replace('<d>','<d1>').replace('<m>','<m1>').replace('<s>','<s1>') +
        u'$', re.X),
    list(u'-+0123456789.'))

# Permette di convertire tutti i valori a radianti
def _toRadiansAll(obj):
    if isinstance(obj, dms) or isinstance(obj, udms):
        return obj.toRadians()
    else:
        return obj

def _map_nested_dicts(ob, func):
    if isinstance(ob, collections.abc.Mapping):
        return {k: _map_nested_dicts(v, func) for k, v in ob.items()}
    elif isinstance(ob, list):
            return [_map_nested_dicts(obj, func) for obj in ob]
    else:
        return func(ob)

def toRadiansAll(measDict):
    return _map_nested_dicts(measDict, _toRadiansAll)

def loadMeasure(data_file):
    # Carica il file yaml
    with open(data_file) as f:
        measDict = yaml.load(f, loader)
        
    # Se il file yaml contiene un campo 'data', lo carica già tramite numpy
    if 'data' in measDict.keys():
        data = np.loadtxt(StringIO(measDict['data']), unpack = True)
        return measDict, data
    else:
        return measDict

def sort_by_column(arr, col):
    return arr[:,arr[col,:].argsort()]

if __name__ == "__main__":
    measDict, data = loadMeasure('./test.yaml')
    print(measDict)
    print(data)
