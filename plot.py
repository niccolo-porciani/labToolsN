#!/bin/python3
# -*- coding:utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from gc_lab_fork.lab import FitCurveOutput, CurveModel

__all__ = [
    'std_fit_plot',
    'fco_fit_plot',
    'do_preamble',
    'gridspec_kw_std']

gridspec_kw_std = {'hspace': 0, 'height_ratios': [3, 1]}

def do_preamble():
    # Questa versione del codice utilizza la distribuzione LaTeX del computer,
    # permettendo ad esempio di usare pacchetti come siunitx, ma è più lenta.
    # Possiamo valutare di usare il typesetting engine interno a matplotlib, vedi:
    # https://matplotlib.org/3.1.1/tutorials/text/mathtext.html

    # Imposta l'utilizzo di LaTeX nei label e titoli dei grafici
    plt.rcParams['text.usetex'] = True

    # Imposta il preambolo LaTeX
    plt.rcParams['text.latex.preamble'] = [r'''
    \usepackage{amsmath}
    \usepackage{siunitx}
    \usepackage[italian]{babel}
    ''']

    # Fa in modo che i bordi siano sensati
    plt.rcParams['figure.autolayout'] = True

    # Imposta il font (Computer Modern è il font di default di LaTeX)
    plt.rcParams['font.family'] = 'serif'
    plt.rcParams['font.serif'] = 'Computer Modern'

    # Imposta la dimensione del font
    plt.rc('font', size=18)

do_preamble()

def std_fit_plot(x, y, dx, dy, par, model, xLabel=None, yLabel=None,
                 resLabel=r'Res. norm.', title=None, xlog=False,
                 ylog=False, center_res=False, showOrigin = False):

    # "Sanitizza" il modello
    if isinstance(model, CurveModel):
        model = CurveModel.f

    # Sanitizza i parametri
    par = np.atleast_1d(par)

    # Crea una figura con una griglia 2 * 1 di subplot.
    # figFit è l'oggetto figura, axFit è un array contenente i due subplot.
    # sharex = True impone che i due subplot condividano l'asse x
    # il dizionario gridspec_kw contiene le istruzioni sulla separazione verticale e sul
    # rapporto delle altezze
    fig, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={
                           'hspace': 0, 'height_ratios': [3, 1]})
    
    # Plot dei dati
    ax[0].errorbar(x, y, xerr=dx, yerr=dy, linestyle='',
                   c='k', capsize=0, elinewidth=0.3)

    # Plot della curva fittata
    basex = np.linspace(np.amin(x), np.amax(x), 1000)
    ax[0].plot(basex, model(basex, *par), c='b', lw=.2)

    # Plot dei residui, con anche la retta orizzontale in 0
    ax[1].plot(x, (y - model(x, *par)) / dy, lw=0, marker='.')
    ax[1].plot(basex, np.full(len(basex), 0), c='b', lw=.2)

    # Centra l'asse verticale del grafico dei residui su 0. Non necessario.
    if center_res:
        ax[1].set_ylim([-np.amax(ax[1].get_ylim()), np.amax(ax[1].get_ylim())])

    if showOrigin:
        ax[0].set_xlim(left = 0)
        ax[0].set_ylim(bottom = 0)

    # Attiva le divisioni minori sui grafici
    ax[0].minorticks_on()
    ax[1].minorticks_on()

    # Attiva, se necessario, le scale logaritmiche
    if xlog:
        ax[0].set_xscale('log')
    if ylog:
        ax[0].set_yscale('log')

    # Imposta i label; l'uso delle raw string è necessario per LaTeX
    if xLabel is not None:
        ax[1].set_xlabel(xLabel)
    if yLabel is not None:
        ax[0].set_ylabel(yLabel)
    if resLabel is not None:
        ax[1].set_ylabel(resLabel)
    if title is not None:
        ax[0].set_title(title)

    return fig, ax

def fco_fit_plot(fco, model, xLabel=None, yLabel=None, resLabel=r'Res. norm.',
                 title=None, xlog=False, ylog=False, center_res=False):
    return std_fit_plot(fco.datax, fco.datay, fco.xerr, fco.yerr, fco.par, model,
                        xLabel=xLabel, yLabel=yLabel, resLabel=r'Res. norm.',
                        title=title, xlog=xlog, ylog=ylog, center_res=center_res)

if __name__ == "__main__":

    from gc_lab_fork.lab import fit_curve

    # Valori di esempio
    V = np.linspace(1, 3, 5)
    I = np.linspace(4, 3, 5)
    dV = np.full(5, .2)
    dI = np.full(5, .2)
    def model_f(x, m, q): return m * x + q
    p0 = [1, 1]

    # Fitta i valori di esempio
    # ~ model = CurveModel(model_f, symb = True, npar = 2)
    model = model_f
    fco = fit_curve(model, V, I, dx=dV, dy=dI, p0=p0, method='wleastsq-lm')

    fig, ax = fco_fit_plot(
        fco, model, xLabel=r'$V\ [\si{\volt}]$', yLabel=r'$I\ [\si{\milli\ampere}]$')

    # Mostra la figura
    plt.show()
