#!/bin/python3
# -*- coding:utf-8 -*-

import numpy as np
import math

__all__ = [
    'osc_fs',
    'osc_fs_max',
    'osc_err',
    'errsum']

def _osc_fs(V, v_pos_div = 0):
    fs = [d * 10 ** e for e in range(-3,1) for d in (1,2,5)][1:]
    
    if V >= 0:
        val = V / (5 - v_pos_div)
    else:
        val = np.absolute(V) / (5 + v_pos_div)
        
    for vpdiv in fs:
        if val < vpdiv:
            return vpdiv
    
    raise RuntimeError("Fondoscala impossibile")

osc_fs = np.vectorize(_osc_fs)

def osc_fs_max(x):
    if hasattr(x, 'size'):
        return _osc_fs(np.amax(x))
    else:
        return _osc_fs(x)
    

def _osc_err(V, v_p_div, v_pos = None, v_pos_div = None, sqerr = True):
    
    errsum = sqerr if hasattr(sqerr, '__call__') else (lambda x, y: np.sqrt(x**2 + y**2)) if sqerr else (lambda x, y: x + y)
    
    if v_pos_div != None:
        if v_pos == None:
            v_pos = v_p_div * v_pos_div
        else:
            raise RuntimeError("Both v_pos formats specified!")
    
    if v_pos == None:
        offs_err = 1e-3
        vdiv_mult = 0.1
    else:
        offs_err = 175e-3 if v_p_div > 200e-3 else 7e-3
        vdiv_mult = 0.2
    
    err = 0.03 * np.absolute(V)
    if v_pos is not None:
        err = errsum(err, 0.04 * np.absolute(v_pos))
    err = errsum(err, vdiv_mult * v_p_div)
    err = errsum(err, offs_err)
    
    return err 
    
osc_err = np.vectorize(_osc_err, excluded = ['sqerr'])

def _errsum(*En):
    err = 0
    for Ei in En:
        err += Ei ** 2
    return np.sqrt(err)

errsum = np.vectorize(_errsum)
