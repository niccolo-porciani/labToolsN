#!/bin/python3
# -*- coding:utf-8 -*-

import numpy as np
import labToolsA.LabTools.latex as lta
import uncertainties.core as ucc


def savetable(path, cols):
    tbl = lta.TabularContent()
    for col in cols:
        tbl.add_column(col)
    tbl.save(path)

def avg_semidisp(a, b):
    return (a + b) / 2, abs(a - b) / 2

def setvar(varobj, varval, varname):
    if isinstance(varval, ucc.Variable) or isinstance(varval, ucc.AffineScalarFunc):
        varobj.setvariable(lta.UDecimal(varname, varval))
    else:
        varobj.setvariable(lta.Decimal(varname, varval))
    return varobj

def setvars(varobj, varvals, varnames):
    for varval, varname in zip(varvals, varnames):
        setvar(varobj, varval, varname)
    return varobj
